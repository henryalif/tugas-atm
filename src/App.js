import React from 'react';
import './App.css'
import {
  Navbar,
  Nav,
  Row,
  Col,
  Image,
  Card,
  Container
} from 'react-bootstrap';

function Navigasi(){
  return (
    <header class="header navbar-fixed-top">
      <Navbar class="navbar"bg="light" expand="lg" classname="container" role="navigation">
      <Navbar sticky="top">
      <Navbar.Brand href="#home">
            <img
              src="http://haus.co.id/img/images/home/Logo%20haus%20Hitam%20Panjang.png"
              width="10%"
              height="10%"
              className="d-inline-block align-top"
              alt=" "
            />
      </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="#home">HOME</Nav.Link>
            <Nav.Link href="#link">TENTANG</Nav.Link>
            <Nav.Link href="#link">LOKASI</Nav.Link>
            <Nav.Link href="#link">KARIR</Nav.Link>
            <Nav.Link href="#link">KONTAK</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      </Navbar>
      </header>
    );
}

function Welcome(){
  return (
    <div>
    <div expand="lg">
      <Row>
        <Col>
          <Image src="http://haus.co.id/img/images/home/Red%20Velvet.jpg" 
          width="100%" 
          height="100%"/>
        </Col>
      </Row>
    </div>
    </div>
    );
}

function Separator(){
  return(
      <div>
        <br></br>
      </div>
  );
}

function Tentang(){
  return (
  <div class="about">
  <container>
    <Row>
      <Col>
        <h2 style={{marginLeft:"50px", marginTop:"100px"}}>
            <b> About HAUS!</b>
        </h2>
        <p style={{marginLeft:"50px", marginTop:"30px"}}>
        PT. Inspirasi Bisnis Nusantara merupakan perusahaan yang bergerak di bidang Food & Beverage dan dikenal dengan brand "Haus!" yang menyediakan minuman dan makanan kekinian yang di gandrungi oleh generasi milenial dengan berbagai macam varian rasa serta harga yang terjangkau.
        </p>
        <p style={{marginLeft:"50px", marginTop:"30px"}}>
        Saat ini Haus! sudah memiliki 113 cabang outlet di Jabodetabek dan Bandung. Selain ekspansi ke kota-kota besar di Indonesia, Haus! juga akan terus melakukan pengembangan dan inovasi.
        </p>
      </Col>
      <Col>
        <Card style={{ width: '40rem' }}>
          <Card.Img variant="top" src="http://haus.co.id/img/images/home/Haus%20mantap.jpg" />
        </Card>
      </Col>
  </Row>
  </container>
  </div>
    );
}

function Separator2(){
  return(
      <div>
        <br></br>
      </div>
  );
}


function Review(){
  const Penilaian = (props) => {
    return(
          <Card style={{ width: '15rem' }} className="kartu">
            <Card.Img variant="top" src={props.image} style={{ width: '100px', marginLeft:'60px', marginTop:'10px'  }}/>
            <Card.Body>
              <Card.Title>{props.title}</Card.Title>
              <Card.Text>
                {props.subtitle}
              </Card.Text>
            </Card.Body>
         </Card>
    );
}

return (
  <Container>
  <Row>
    <Col>
          <Penilaian
            title="Nex Carlos"
            subtitle="Mantep banget mamen, Cheese enak banget, creamy rasa manisnya pas enak banget mamen"
            image="http://haus.co.id/img/images/home/testimonial_1.png"
          />
    </Col>
    <Col>
          <Penilaian
            title="Ria Ricis"
            subtitle="Hmm asli seger banget, Bahan bakunya berkualitas, harganya murah murah lagi cuss ke haus!"
            image="http://haus.co.id/img/images/home/testimonial_2.png"
          />
    </Col>
    <Col>
          <Penilaian
            title="Mgdalenaf"
            subtitle="BOBA 14 RIBU Mereka buat bobanya sendiri, empuk dan kenyalnya pas. Enak banget pokoknya wajib kalian coba !"
            image="http://haus.co.id/img/images/home/testimonial_4.png"
          />
    </Col>
    <Col>
          <Penilaian
            title="Tanboy Kun"
            subtitle="Gw cobain choco lava milo, bener bener berasa milo nya cocok buat kalian yang suka manis karena manis nya pas"
            image="http://haus.co.id/img/images/home/testimonial_3.png"
          />
    </Col>
  </Row>
  </Container>
);
}

function Separator3(){
    return(
        <div>
          <br></br>
        </div>
    );
  }

  function Footer(){
    return(
      <div className="background">
      <container className="con-bg" fluid>
        <Row>
          <Col xs={6} md={4}>
            <h1><b>HAUS!</b></h1><br></br>
            <p>Mohon Maaf, Kita Sudah Tidak Membuka Kemitraan ;)</p>
          </Col>

          <Col xs={6} md={4}>
          <div className="teks1">
            <ul class="list-unstyled footer-list">
              <li><p>Home</p></li>
              <li><p>Lokasi Store</p></li>
              <li><p>Kontak</p></li>
            </ul>
          </div>
          </Col>
          <Col xs={6} md={4}>
          <div className="teks2">
            <ul class="list-unstyled footer-list">
              <li><p>Kritik dan saran</p></li>
              <li><p>Promo</p></li>
              <li><p>Forum</p></li>
            </ul>
          </div>
          </Col>
        </Row>
      </container>
      </div>
    );
  }


function App(){
  return(
      <div>
        <Navigasi/>
        <Welcome/>
        <Separator/>
        <Tentang/>
        <Separator2/>
        <Review/>
        <Separator3/>
        <Footer/>
      </div>
    );
}

export default App;